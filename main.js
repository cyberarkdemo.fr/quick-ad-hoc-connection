function api_authenticate() {

    var pvwa_address = document.getElementById("pvwa_address").value;
    var username = document.getElementById("cybr_username").value;
    var password = document.getElementById("cybr_password").value;
    console.log("PVWA Address: " + pvwa_address)

    var xhr = new XMLHttpRequest();
    var url = 'https://' + pvwa_address + '/PasswordVault/API/Auth/CyberArk/Logon';
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            api_token = this.responseText.replace(/"/g, "");
            document.getElementById("api_token").value = api_token;
            document.getElementById("adhoc_username").value = username;
            document.getElementById("adhoc_password").value = password;
        }
    }

    xhr.send(JSON.stringify({
            UserName: username,
            Password: password,
            Type: 'cyberark'
        }));
}

function download(filename, text) {
    
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
}

function adhoc_connect() {

    var xhr = new XMLHttpRequest();
    var pvwa_address = document.getElementById("pvwa_address").value;
    var api_token = document.getElementById("api_token").value;
    var username = document.getElementById("adhoc_username").value;
    var password = document.getElementById("adhoc_password").value;
    var address = document.getElementById("address").value;
    var url = 'https://' + pvwa_address + '/PasswordVault/API/Accounts/AdHocConnect';

    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader('Authorization', api_token);

    xhr.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log(this.responseText);
            download(""+ username + "@"  + address + ".rdp", this.responseText)
        }
    }

    payload = {
        "secret": password,
        "address": address,
        "userName": username,
        "platformId": "PSMSecureConnect",
        "PSMConnectPrerequisites": {
            "ConnectionComponent": "PSM-RDP",
            "ConnectionParams": {}
        },
        "extraFields": {}
    }

    xhr.send(JSON.stringify(payload));
}
